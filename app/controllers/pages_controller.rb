class PagesController < ApplicationController
  rescue_from Exception, with: :exception

  def index
    @videos = Video.all
  end

  def edit
    @concat_video_blob = Video.concat_videos(params[:files], params[:resolutions])
    @ffprobe_json = Ffprober::Parser.from_file(ActiveStorage::Blob.service.send(:path_for, @concat_video_blob.key)).json
  end

  def create
    blob = Video.process_video(params[:name], params[:format], params[:file], params[:width], params[:height], params[:starting] || [], params[:ending] || [])

    video = Video.new(name: params[:name])
    video.file.attach(blob)
    video.save!

    redirect_to root_path
  end

  def delete
    video = Video.find(params[:id])
    video.file.purge
    video.destroy

    redirect_to root_path
  end

  private
  def exception
    redirect_to root_path
  end
end
