class Video < ApplicationRecord
  has_one_attached :file

  def self.concat_videos(files, resolutions)
    unless resolutions.blank?
      w = FFMPEG::Movie.new(files[resolutions[0].to_i - 1].path.to_s).width
      h = FFMPEG::Movie.new(files[resolutions[0].to_i - 1].path.to_s).height
    else
      w = FFMPEG::Movie.new(files[0].path.to_s).width
      h = FFMPEG::Movie.new(files[0].path.to_s).height
    end

    code = rand(36**15).to_s(36)
    ext = File.extname(files[0].original_filename)
    blobs = []
    files.each_with_index do |file, i|
      ext = File.extname(file.original_filename)
      blob = ActiveStorage::Blob.create_after_upload!(io: file, filename: "#{code}-#{i}#{ext}")
      blobs << blob
    end

    paths = []
    blobs.each_with_index do |blob, i|
      str = "ffmpeg "
      blob_path = ActiveStorage::Blob.service.send(:path_for, blob.key)
      str += "-i #{blob_path} -vf 'scale=#{w}:#{h}:force_original_aspect_ratio=decrease,pad=#{w}:#{h}:x=(#{w}-iw)/2:y=(#{h}-ih)/2:color=black,setsar=1:1' -vb 20M -y #{code}#{ext}"
      system str
      blob.purge

      if (FFMPEG::Movie.new("#{code}#{ext}").audio_stream.blank?)
        str = "ffmpeg -f lavfi -i anullsrc=channel_layout=stereo:sample_rate=44100 -i #{code}#{ext} -c:v copy -c:a aac -shortest #{code}-#{i}#{ext}"
      else
        str = "ffmpeg -i #{code}#{ext} #{code}-#{i}#{ext}"
      end
      system str

      paths << "#{code}-#{i}#{ext}"
    end

    str = "ffmpeg "
    paths.each do |path|
      str += "-i #{path} "
    end

    str += "-filter_complex '"
    paths.count.times do |i|
      str += "[#{i}:v][#{i}:a]"
    end
    str += "concat=n=#{paths.count}:v=1:a=1 [v] [a]' -map '[v]' -map '[a]' -vb 20M -y #{code}#{ext}"
    system str

    path = Rails.root.join("#{code}#{ext}")
    file = File.open(path, "rb")
    blob = ActiveStorage::Blob.create_after_upload!(io: file, filename: "#{code}#{ext}")
    File.delete(path)
    paths.each { |path| File.delete(path) }

    blob
  end

  def self.process_video(name, format, blob_key, width, height, starting, ending) # more options to come
    tmp_blob = ActiveStorage::Blob.find_by(key: blob_key)
    tmp_path = ActiveStorage::Blob.service.send(:path_for, blob_key)

    unless starting.blank?
      str = "ffmpeg "
      paths_to_delete = []

      starting.count.times.each do |i|
        path = "#{blob_key}-#{i}#{format}"
        system "ffmpeg -i #{tmp_path} -ss #{starting[i]} -to #{ending[i]} -vb 20M #{path}"
        str += "-i #{path} "
        paths_to_delete << path
      end

      str += "-filter_complex '"
      starting.count.times do |i|
        str += "[#{i}:v][#{i}:a]"
      end
      str += "concat=n=#{starting.count}:v=1:a=1 [v] [a]' -map '[v]' -map '[a]' -s #{width}x#{height} -vb 20M #{name}#{format}"
      system str

      paths_to_delete.each do |path|
        File.delete(path)
      end
    else
      str = "ffmpeg -i #{tmp_path} -s #{width}x#{height} -vb 20M #{name}#{format}"
      system str
    end

    tmp_blob.purge

    output_path = Rails.root.join("#{name}#{format}")
    file = File.open(output_path, "rb")
    blob = ActiveStorage::Blob.create_after_upload!(io: file, filename: "#{name}#{format}")

    File.delete(output_path)

    blob
  end
end
