Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'pages#index'
  post 'edit', to: 'pages#edit', as: 'edit'
  post 'create', to: 'pages#create', as: 'create'
  delete 'delete/:id', to: 'pages#delete', as: 'delete'
end
