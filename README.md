# README

This is just to test and learn manipulation of video with ffmpeg

Things implemented:

* Uploading multiple video files to concat into a single video file.

* Editing the filename, format, width and height of the video.

* Cutting the concatenated video in to multiple parts and remapping it into a single video.
